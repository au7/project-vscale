.PHONY: create-srv get-ip-address inst-docker deploy-app

create-srv:
	ansible-playbook playbook.yaml -c local --tags crsrv

get-ip-address:
	ansible-playbook playbook.yaml -c local --tags getsrvip

inst-docker:
	ansible-playbook playbook.yaml --tags instdock --ssh-extra-args="-o IdentitiesOnly=yes"

deploy-app:
	ansible-playbook playbook.yaml --tags depapp --ssh-extra-args="-o IdentitiesOnly=yes"

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
