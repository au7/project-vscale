**Тестовый проект: Разворачивание django приложения на VPS Vscale. NO PRODUCTION!!! с помощью ansible**

**Test project: Deploying a django application on VPS Vscale. NO PRODUCTION!!! using ansible**

1. Необходимо создать виртуальное окружение для Вашей версии python например:
    `virtualenv --python=/opt/python3.9.0/bin/python3.9 ~/ansible-vscale`
    и выполнить активацию окружения
    `source ~/ansible-vscale/bin/activate`

2. Установить в этом окружении ansible
   `pip install ansible`

3. Установить в этом окружении ansible-galaxy:
   `ansible-galaxy collection install community.general`

4. Внутри каталога `~/ansible-vscale` клонируйте репозиторий:
    `git clone git@gitlab.com:au7/project-vscale.git`
    и перейдите в него

5. Измените переменные окружения для доступа к Вашему серверу Vscale:
    в файле `group_vars/all.yaml`
    `export VS_API_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx`
    `num_ssh_key: xxxxx число`

-   Они должны у Вас быть для подключения к Vsale по API.
   `ssh_key` это номер Вашего ключа на Vscale
-   Посмотреть номера всех ключей можно имея VS_API_KEY.
    `curl 'https://api.vscale.io/v1/sshkeys' -H 'X-Token: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'`

6. Выполните установку сервера:
    `make create-srv`

7. Посмотреть, полученный сервером внешний ip адрес:
    `make get-ip-address`

8. Итак, у Вас есть ip адрес сервера. Настройте к нему доступ по ssh и проверьте!
    `~/.ssh/config`
    имя сервера, в файле hosts test-django

9. Запустите установку docker на удаленном сервере
    `make inst-docker`

10. Запустите установку приложения на удаленном сервере
    `make deploy-app`
- Код проекта автоматически скачаестя с `github https://github.com/atrucks/test-django-app` при установке
- После успешного прохождения установки можно зайти на приложение по ip:8000
- Можно прописать ip адрес сервер в переменной host: xxx.xxx.xxx.xxx и зайти в приложение без ошибочных сообщений
- Вместо ip адреса можно прописать имя хоста, внести соответствующую запись в Ваш DNS сервер и зайти в приложение по имени хоста.


11. Изменения в переменных сервера осуществляются через установку значений в файле `group_vars/hosts-vscale`
