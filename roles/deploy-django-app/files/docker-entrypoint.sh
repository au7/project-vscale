#!/bin/sh -x

echo "Waiting for PostgreSQL UP..."

while ! nc -z db 5432; do
  sleep 5
done

echo "PostgreSQL started!"

python manage.py flush --no-input
python manage.py migrate --noinput || exit 1
#python manage.py collectstatic --no-input --clear

python manage.py runserver 0.0.0.0:8000
